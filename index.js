const express = require("express");
const cors = require("cors");
const app = express();
const mongoose = require("mongoose");
var bodyParser = require("body-parser");
const morgan = require("morgan");
const dotenv = require("dotenv");
const userRoute = require("./routes/user");
const loginRoute = require("./routes/login");
const passwordRoute = require("./routes/changePassword");
const noteRoute = require("./routes/note");

dotenv.config();
//CONNECT DATABASE
mongoose.connect(process.env.MONGODB_URL, () =>
  console.log("Connected to MongoDB")
);

app.use(bodyParser.json({ limit: "50mb" }));
app.use(cors());
app.use(morgan("common"));

app.get("/api", (req, res) => {
  res.status(200).json("hello");
});

//Route

app.use("/api/users/me", userRoute);

//register new user
app.use("/api/auth/register", userRoute);

//login user
app.use("/api/auth/login", loginRoute);

//Note user
app.use("/api/notes", noteRoute);

//Change password
app.use("/api/users/me", passwordRoute);

app.listen(8080, () => {
  console.log("server is running...");
});
