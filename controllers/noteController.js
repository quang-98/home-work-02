const { Note, User } = require("../model/model");
const jwt = require("jsonwebtoken");
const url = require("url");

const noteController = {
  //Add note
  addNote: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      const createdDate = new Date().toISOString();
      const newNote = new Note({
        ...req.body,
        createdDate: createdDate,
        userId: userId,
        completed: false,
      });

      await newNote.save();
      res.status(200).json({ message: "Success" });
    } catch (err) {
      console.log(err);
      res.status(500).json({ message: "string" });
    }
  },

  // GET user's notes
  getNote: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      const offset = parseInt(req.query.offset);
      const limit = parseInt(req.query.limit) + offset;
      const notes = await Note.find({ userId: userId });
      console.log(limit);

      res.status(200).json({
        offset: offset,
        limit: limit,
        count: limit,
        notes: notes.slice(offset, limit),
      });
    } catch (err) {
      console.log(err);
      res.status(500).json({ message: "string" });
    }
  },

  //DELETE user's note by id
  deleteNote: async (req, res) => {
    try {
      console.log(req.params.id);
      await Note.deleteOne({ _id: req.params.id });

      res.status(200).json({ message: "success" });
    } catch (err) {
      console.log(err);
      res.status(500).json({ message: "string" });
    }
  },

  //Get Note by id
  getNoteId: async (req, res) => {
    try {
      const notes = await Note.find({ _id: req.params.id });
      res.status(200).json(notes);
    } catch (err) {
      console.log(err);
      res.status(500).json({ message: "string" });
    }
  },

  //Update user's note
  updateNote: async (req, res) => {
    try {
      const text = req.body;
      await Note.findByIdAndUpdate(req.params.id, text);
      res.status(200).json({ message: "success" });
    } catch (err) {
      console.log(err);
      res.status(500).json({ message: "string" });
    }
  },

  //Check and uncheck Note
  checkNote: async (req, res) => {
    try {
      const notes = await Note.find({ _id: req.params.id });
      if (!notes[0].completed) {
        await Note.updateOne(
          { _id: req.params.id },
          { $set: { completed: true } }
        );
      } else {
        await Note.updateOne(
          { _id: req.params.id },
          { $set: { completed: false } }
        );
      }

      res.status(200).json({ message: "success" });
    } catch (err) {
      console.log(err);
      res.status(500).json({ message: "string" });
    }
  },
};

module.exports = noteController;
