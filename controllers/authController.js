const { Note, User } = require("../model/model.js");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const authController = {
  // create newUser
  createUser: async (req, res) => {
    const { username, password: plainTextPassword } = req.body;

    if (!username || typeof username !== "string") {
      return res.json({ message: "Invalid password" });
    }

    if (!plainTextPassword || typeof plainTextPassword !== "string") {
      return res.json({ message: "Invalid password" });
    }

    if (plainTextPassword.length < 5) {
      return res.json({
        message: "Password too small. Should be atleast 6 characters",
      });
    }
    const password = await bcrypt.hash(plainTextPassword, 10);

    try {
      const createdDate = new Date().toISOString();
      const newUser = new User({
        username: username,
        password: password,
        createdDate,
      });

      await newUser.save();

      res.status(200);
    } catch (err) {
      if (err.code === 11000) {
        return res.json({ message: "User is already used" });
      }
      throw err;
    }

    res.json({ message: "Success" });
  },

  //Login User

  loginUser: async (req, res) => {
    const { username, password } = req.body;

    const user = await User.findOne({ username }).lean();

    if (!user) {
      return res.json({ message: "Invalid username/password" });
    }

    if (await bcrypt.compare(password, user.password)) {
      const token = jwt.sign(
        { id: user._id, username: user.username },
        process.env.JWT_SECRET
      );

      return res.json({ message: "Success", jwt_token: token });
    }

    res.json({ message: "error" });
  },
};

module.exports = authController;
