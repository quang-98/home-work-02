const { Note, User } = require("../model/model.js");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const userController = {
  //GET User
  getUser: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;

    try {
      const users = await User.findById(userId);

      res.status(200).json({
        user: {
          id: users._id,
          username: users.username,
          createdDate: users.createdDate,
        },
      });
    } catch (err) {
      res.status(500).send({ message: "string" });
    }
  },

  //Change password
  changePassword: async (req, res) => {
    const { oldPassword, newPassword: plainTextPassword } = req.body;

    if (!plainTextPassword || typeof plainTextPassword !== "string") {
      return res.json({ message: "Invalid password" });
    }

    if (plainTextPassword.length < 5) {
      return res.json({
        message: "Password too small. Should be atleast 6 characters",
      });
    }
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const _id = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      const user = await User.findById(_id);

      if (await bcrypt.compare(oldPassword, user.password)) {
        console.log(plainTextPassword);

        const password = await bcrypt.hash(plainTextPassword, 10);

        await User.updateOne(
          { _id },
          {
            $set: { password },
          }
        );

        res.status(200).json({ message: "success" });
      } else res.status(400).json({ message: "wrong password" });
    } catch (err) {
      console.log(err);
      res.json({ message: "error" });
    }
  },

  //DELETE User
  deleteUser: async (req, res) => {
    const accessToken = req.headers.token;
    const token = accessToken.split(" ")[1];
    const userId = jwt.verify(token, process.env.JWT_SECRET).id;
    try {
      await User.deleteOne({ _id: userId });
      res.status(200).send({ message: "Success" });
    } catch (err) {
      res.status(500).send({ message: "error" });
    }
  },
};
module.exports = userController;
