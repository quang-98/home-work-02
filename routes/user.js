const userController = require("../controllers/userController");
const authController = require("../controllers/authController");
const middlewareController = require("../controllers/middlewareController");

const router = require("express").Router();

//create user
router.post("/", authController.createUser);

//get user
router.get("/", middlewareController.verifyToken, userController.getUser);

//delete user
router.delete("/", middlewareController.verifyToken, userController.deleteUser);

module.exports = router;
