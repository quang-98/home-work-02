const authController = require("../controllers/authController");

const router = require("express").Router();

//create user
router.post("/", authController.loginUser);

module.exports = router;
