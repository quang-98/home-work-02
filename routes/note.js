const noteController = require("../controllers/noteController");
const middlewareController = require("../controllers/middlewareController");

const router = require("express").Router();

//ADD note
router.post("/", middlewareController.verifyToken, noteController.addNote);

//GET note by id
router.get("/:id", middlewareController.verifyToken, noteController.getNoteId);

//GET note of user
router.get("/", middlewareController.verifyToken, noteController.getNote);

//DELETE use's note by id
router.delete(
  "/:id",
  middlewareController.verifyToken,
  noteController.deleteNote
);

//UPDATE user's note by id
router.put("/:id", middlewareController.verifyToken, noteController.updateNote);

//CHECK user by id
router.patch(
  "/:id",
  middlewareController.verifyToken,
  noteController.checkNote
);

module.exports = router;
