const userController = require("../controllers/userController");
const authController = require("../controllers/authController");
const middlewareController = require("../controllers/middlewareController");
const router = require("express").Router();

router.patch(
  "/",
  middlewareController.verifyToken,
  userController.changePassword
);

module.exports = router;
